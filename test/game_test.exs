defmodule ExMon.GameTest do
  use ExUnit.Case

  alias ExMon.{Game, Player}

  describe "start/2" do
    test "starts the game state" do
      player = Player.build("Ryu", :chute, :soco, :cura)
      computer = Player.build("Robot", :chute, :soco, :cura)

      assert {:ok, _pid} = Game.start(computer, player)
    end
  end

  describe "info/0"do
    test "returns the current game state" do
      player = Player.build("Ryu", :chute, :soco, :cura)
      computer = Player.build("Robot", :chute, :soco, :cura)

      Game.start(computer, player)

      expected_response = %{
        computer: %ExMon.Player{
          life: 100, 
          moves: %{move_avg: :soco, move_heal: :cura, move_rnd: :chute}, 
          name: "Robot"
        }, 
        player: %ExMon.Player{
          life: 100, 
          moves: %{move_avg: :soco, move_heal: :cura, move_rnd: :chute}, 
          name: "Ryu"}, status: :started, turn: :player
        }

      assert Game.info() == expected_response
    end
  end

  describe "update/1" do
    test "returns the game status updated" do
      player = Player.build("Ryu", :chute, :soco, :cura)
      computer = Player.build("Robot", :chute, :soco, :cura)

      Game.start(computer, player)

      new_state = %{
        computer: %ExMon.Player{
          life: 85, 
          moves: %{move_avg: :soco, move_heal: :cura, move_rnd: :chute}, 
          name: "Robot"
        }, 
        player: %ExMon.Player{
          life: 72, 
          moves: %{move_avg: :soco, move_heal: :cura, move_rnd: :chute}, 
          name: "Ryu"}, status: :started, turn: :player
        }

        expected_response = %{new_state | turn: :computer, status: :continue}

        Game.update(new_state)

        assert expected_response == Game.info()
    end
  end
end
