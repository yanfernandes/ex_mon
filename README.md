# ExMon

<p align="center"><img src="http://img.shields.io/static/v1?label=STATUS&message=CONCLUÍDO&color=GREEN&style=for-the-badge"/></p>


Um jogo de batalha entre jogador e computador criado em Elixir. Esse é o ExMon!

O processo de construção do jogo começa com a opção de criar um personagem com **nome (name), movimento moderado (move_avg), movimento aleatório (move_rnd) e poder de cura (heal).** Após criar seu personagem, é possível criar os golpes e travar a batalha. 

O valor inicial de vida (life) é de 100 pontos, e o primeiro que conseguir deixar o oponente com valor de 0, vence a partida

## Installation

Partindo da premissa que você já tenha o Elixir instalado na sua máquina, faça um clone deste repositório e instale as dependências

`mix deps.get`

Utilize o iex no seu terminal, rodando dentro do contexto da nossa aplicação.

```
cd /ex_mon
iex -S mix
```

O primeiro passo é criar o seu player. Utilize o comando abaixo:

`player = ExMon.create_player("player_name", :soco, :chute, :cura)`

Inicie o jogo, usando o comando start_game:

`ExMon.start_game(player)`

Com o jogo já iniciado, agora é hora de começar o ataque! Perceba que o status é atualizado a cada rodada, descritos em: **started_game, continue e game_over**.
Para atacar, utilize o seguinte comando:

`ExMon.make_move(:soco)`

Existem intensidades de golpes(movimentos). Golpes moderados tem poder de 18 a 25 pontos, enquanto que golpes aleatórios tem intervalo de 10 a 35 pontos.

Utilize também o movimento de cura! Isso ajuda a recuperar pontos quando precisar. Cura também tem um intervalo de 18 a 25 pontos:

`ExMon.make_move(:soco)`

